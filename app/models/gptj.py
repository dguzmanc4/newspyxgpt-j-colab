import time
import json
import torch
import transformers
from loguru import logger
from models.message import Message
from transformers import GPTJForCausalLM, AutoTokenizer
from utils.utils import GPTJBlock, GPTJModel, GPTJForCausalLM, FrozenBNBEmbedding, DequantizeAndLinear, FrozenBNBLinear

class Gptj:
    def __init__(self):
        self.init_transformers()
        self.config = transformers.GPTJConfig.from_pretrained("EleutherAI/gpt-j-6B")
        self.tokenizer = transformers.AutoTokenizer.from_pretrained("EleutherAI/gpt-j-6B")
        self.gpt = GPTJForCausalLM.from_pretrained("/content/newspyxgpt-j/colab_storage/saved_models/gpt-j-6B-8bit", low_cpu_mem_usage=True)
        self.device = 'cuda' if torch.cuda.is_available() else 'cpu'
        self.gpt.to(self.device)

    def init_transformers(self):
        transformers.models.gptj.modeling_gptj.GPTJBlock = GPTJBlock
    
    def train_model(self):
        pass

    def save_model(self):
        save_dir = "./colab_storage/saved_models/gpt-j-6B-8bit"
        self.gpt.save_pretrained(save_dir)
    
    def generate_news(self, message:Message):
        prompt = message.input
        temperature = float(message.temperature)
        length = int(message.length)

        start = time.time()
        prompt = self.tokenizer(prompt, return_tensors='pt')
        prompt = {key: value.to(self.device) for key, value in prompt.items()}
        out = self.gpt.generate(**prompt, min_length=length, max_length=length, temperature=temperature, do_sample=True)
        generated_text = self.tokenizer.decode(out[0])
        message.output = generated_text
        end_time = time.time() - start

        result = {
            "output" : message.output,
            "time taken": f'{round(end_time,2)} Seconds'
        }

        return result