from pydantic import BaseModel

class Message(BaseModel):
    input: str = None
    output: dict = None
    length: str = None
    temperature: str = None
