import os
import uvicorn
from pyngrok import ngrok
from loguru import logger
from config import settings 
from fastapi import FastAPI
from models.gptj import Gptj
from models.message import Message
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()
gptj = Gptj()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Newspy x GPT-J!"}

@app.post("/prompt/", tags=["Prompt"])
async def generate(message: Message):
    return gptj.generate_news(message)

if __name__ == "__main__":
    os.system(f"kill -9 $(lsof -t -i:{settings.PORT})")
    if settings.USE_NGROK:
        ngrok.set_auth_token("2AqwLWalKpvD9qVuAONRGFTeetu_VmrzfsRPeFQ6dPyLNgHm")
        ngrok_tunnel = ngrok.connect(settings.PORT)
        logger.info("Ngrok url: {}".format(ngrok_tunnel.public_url))
    uvicorn.run(app, host=settings.HOST, port=settings.PORT, log_level="info")