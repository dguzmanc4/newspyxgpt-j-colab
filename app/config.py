import os
from pydantic import BaseSettings

class ServerSettings(BaseSettings):
    APP_NAME: str = "NEWSPYxGPT-J"
    HOST: str = "0.0.0.0"
    PORT: int = 8000
    USE_NGROK: str = os.environ.get("USE_NGROK", "True") 

class Settings(ServerSettings):
    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"

settings = Settings()    