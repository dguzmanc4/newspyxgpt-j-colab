# Google Colab
import os

# 1.Install colabcode
os.system('pip install colabcode==0.3.0')

# 2.Install gcsfuse
os.system('echo "deb http://packages.cloud.google.com/apt gcsfuse-bionic main" > /etc/apt/sources.list.d/gcsfuse.list')
os.system('curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -')
os.system('apt -qq update')
os.system('apt -qq install gcsfuse')

# 3.Ngrok Tunnel
os.system('wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip')
os.system('unzip -o ngrok-stable-linux-amd64.zip')
os.system('./ngrok authtoken 2AocxlTXYfQOYaJ1bpnGJqFaAFr_5QM2gHaboePoKYCQAqYtw')

# 4.Mount Bucket
CLOUD_STORAGE_BUCKET_NAME = 'newspy_storage'
COLAB_STORAGE_NAME = 'colab_storage'

os.system(f'mkdir -p {COLAB_STORAGE_NAME}')
os.system(f'gcsfuse --implicit-dirs --limit-bytes-per-sec -1 --limit-ops-per-sec -1 {CLOUD_STORAGE_BUCKET_NAME} {COLAB_STORAGE_NAME}')

# 5.Start CodeServer
#from colabcode import ColabCode
#ColabCode(port=10000, password='ColabPro')

# ---------------------> Step 4 and 5 <------------------- #
"""
!git clone https://gitlab.com/dguzmanc4/newspyxgpt-j.git
%cd newspyxgpt-j/
!python start_colab.py

# 4.Mount Bucket
CLOUD_STORAGE_BUCKET_NAME = "newspy_storage"
COLAB_STORAGE_NAME = "colab_storage"
# Auth google
import os
from google.colab import auth
auth.authenticate_user()

os.system(f'mkdir -p {COLAB_STORAGE_NAME}')
os.system(f'gcsfuse --implicit-dirs --limit-bytes-per-sec -1 --limit-ops-per-sec -1 {CLOUD_STORAGE_BUCKET_NAME} {COLAB_STORAGE_NAME}')

# 5.Start CodeServer
from colabcode import ColabCode
ColabCode(port=10000, password="colab")

# Librerias permanentes
# Se agrega la ruta de librerias alojadas en Cloud Storage
import sys
sys_path = f'{COLAB_STORAGE_NAME}/libraries'
sys.path.append(sys_path)
#sys.path.insert(0, sys_path)
!pip install --target=$sys_path -q git+https://github.com/huggingface/transformers.git
"""

#pipreqs manage requirements
#os.system('./ngrok start --config=./ngrok.yml --all')

""""
save_dir = "./colab_storage/saved_models/gpt-j-6B-8bit"
gpt.save_pretrained(save_dir)

!pip install transformers==4.14.1
!pip install bitsandbytes-cuda111==0.26.0
!pip install datasets==2.3.2
!pip install torch==1.11.0
!pip install loguru==0.6.0
!pip install accelerate==0.10.0
"""